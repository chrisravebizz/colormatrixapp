package com.example.colormatrixapp.model.remote

interface ColorApi {
    suspend fun getRandomColor(color: Int) : List<Int>
}