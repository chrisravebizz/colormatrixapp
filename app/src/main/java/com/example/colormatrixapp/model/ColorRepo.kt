package com.example.colormatrixapp.model

import com.example.colormatrixapp.model.remote.ColorApi
import com.example.colormatrixapp.randomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ColorRepo {

    private val colorApi = object : ColorApi {
        override suspend fun getRandomColor(color: Int): List<Int> {
            var newColors: MutableList<Int> = mutableListOf()

            repeat(color) {
                newColors.add(randomColor)
            }
            return newColors
        }
    }

    suspend fun randomColor(color: Int) : List<Int> = withContext(Dispatchers.IO) {
        colorApi.getRandomColor(color)
    }
}