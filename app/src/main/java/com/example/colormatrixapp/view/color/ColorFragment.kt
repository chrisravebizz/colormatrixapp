package com.example.colormatrixapp.view.color

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.colormatrixapp.databinding.FragmentColorBinding
import com.example.colormatrixapp.viewmodel.ColorViewModel

/**
 * Step 1: Add RecyclerView to the Layout
 * Step 2: Setup RecyclerView LayoutManager
 * Step 3: Setup RecyclerView Adapter
 * Step 4: Connect RecyclerView to Custom Adapter
 */
class ColorFragment : Fragment() {

    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!
    private val colorsViewModel by viewModels<ColorViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnFetchColor.setOnClickListener {
            binding.rvList.layoutManager = GridLayoutManager(context, 3)
            binding.rvList.adapter = ColorAdapter().apply {
                val color = binding.etColors.text.toString().toInt()
                colorsViewModel.getRandomColor(color)
                colorsViewModel.randomColor.observe(viewLifecycleOwner) {
                    addColors(it)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}