package com.example.colormatrixapp.view.color

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.colormatrixapp.databinding.ItemColorBinding

/**
 * Step 1: Extend RecyclerView.Adapter
 * Step 2: Create/Setup Custom ViewHolder and Pass into RecyclerView.Adapter
 * Step 3: Override required methods for Adapter onCreateViewHolder, onBindViewHolder, getItemCount
 * Step 4: Setup your list of data to display
 */
class ColorAdapter : RecyclerView.Adapter<ColorAdapter.ColorViewHolder>() {

    private var color = mutableListOf<Int>()

    /* Set up View Holder */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        val binding = ItemColorBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ColorViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        val color = color[position]
        holder.loadColor(color)
    }

    override fun getItemCount(): Int {
        return color.size
    }

    fun addColors(color: List<Int>) {
        this.color = color.toMutableList()
        notifyDataSetChanged()
    }

    class ColorViewHolder(
        private val binding: ItemColorBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColor(name: Int) {
            binding.ivColor.setBackgroundColor(name)
        }
    }

}
