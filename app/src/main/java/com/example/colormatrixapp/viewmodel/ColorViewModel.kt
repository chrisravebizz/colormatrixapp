package com.example.colormatrixapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.colormatrixapp.model.ColorRepo
import kotlinx.coroutines.launch

class ColorViewModel : ViewModel() {

    private val repo = ColorRepo

    private val _randomColor = MutableLiveData<List<Int>>()
    val randomColor: LiveData<List<Int>> get() = _randomColor

    fun getRandomColor(color: Int) {
        viewModelScope.launch {
            val randomColor = repo.randomColor(color)
            _randomColor.value = randomColor
        }
    }
}